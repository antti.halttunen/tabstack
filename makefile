prog = tabstack
out  = out
dist = $(out)/$(prog)

all:
	@mkdir -p $(dist)
	$(MAKE) -C icons
	@cp icons/out/*.png manifest.json $(prog).js $(dist)

clean:
	$(MAKE) -C icons clean
	@rm -rf $(out)
