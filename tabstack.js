chrome.commands.onCommand.addListener( (_) => {
    chrome.tabs.query({ currentWindow: true }, tabs => {
        let n = tabs.length
        if (n>1) {
            let t = tabs.find( t => t.active )
            let i = t.index
            let k = (i<(n-1)) ? (i+1) : (i-1)
            let a = tabs[k]

            chrome.tabs.move(  t.id, { index:  0    })
            chrome.tabs.update(a.id, { active: true })
        }
    })
})
