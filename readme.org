* tabstack

tabstack is a Google Chrome extension that moves the current tab to the first
position while keeping the focus at the current position.

[a b (c) d e] -> [c a b (d) e]

NOTE: does not support pinned tabs.

** configure

the default key is Ctrl-I (^i), configurable via the chrome://extensions
"Keyboard shortcuts" main menu

** install

- clone this repo
- make
- in Chrome, go to chrome://extensions
- choose Load Unpacked
- open the directory file:out/tabstack
